package extractfromsourcecode.handlers;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.WhileStatement;














import java.util.*;

public class DoVisitAST 
{
	
	private List funList ;
	private ArrayList<MethodList> methodlists;
	private ArrayList<String> packagelist;
	private CSVUtil csvObject;
	DoVisitAST()
	{
		csvObject = new CSVUtil();
		funList = new ArrayList();
		methodlists = new ArrayList<MethodList>();
		packagelist = new ArrayList<String>();
		
		packagelist.add("java.lang.String");
		packagelist.add("java.util.ArrayList");
		packagelist.add("java.util.LinkedList");
		packagelist.add("java.util.HashSet");
		packagelist.add("java.util.HashMap");
		packagelist.add("java.util.Stack");
		packagelist.add("java.util.Vector");
		packagelist.add("java.util.LinkedHashMap");
		packagelist.add("java.util.LinkedHashSet");
		packagelist.add("java.util.Iterator");
		packagelist.add("java.util.ListIterator");
	}
	
	public boolean judgePackage(String packagename)
	{
		
		for(int i = 0;i<packagelist.size();i++)
		{
			if(packagename.trim().startsWith(packagelist.get(i)))
				return true;	
		}
		return false;
	}
	
	//visit AST
	public void visitAST(final CompilationUnit unit,final String fileName)
	{
		//ArrayList<MyMethod> classMethodList = new ArrayList<MyMethod>();
		final MethodList invocatiion_method = new MethodList();
		ASTVisitor visitor = new ASTVisitor() 
		{
			//public ArrayList<IfStatement> ifList = new ArrayList<IfStatement>();
			//访问函数声明
			MethodDeclaration methodDeclarNode;
		    //访问invaction
		    public boolean visit(final MethodDeclaration node) 
		    {
		    	methodDeclarNode = node;
		       	IMethodBinding method = (IMethodBinding)node.resolveBinding();
		       	
		       	ASTNode methodParentNode = node;
		       	boolean isAnonymousClass = false;
		       	while(!methodParentNode.getParent().equals(unit.getRoot()))
		       	{
		       		if(methodParentNode.getParent() instanceof AnonymousClassDeclaration)
		       			isAnonymousClass = true;
		       		methodParentNode = methodParentNode.getParent();
		       	}
		       	if(isAnonymousClass)
		       		return true;
		       	if(method != null &&method.getDeclaringClass() != null )
		    	{	    		 
		       		invocatiion_method.methodName = method.getName().toString();
		       		invocatiion_method.methodPackageName = method.getDeclaringClass().getQualifiedName().toString();
		       		invocatiion_method.methodDeclaration = method.getMethodDeclaration().toString();
		       		//invocatiion_method.methodArguments.addAll(node.parameters());
		       		invocatiion_method.methodDeclarationClass = method.getDeclaringClass().getQualifiedName(); 
		       		System.out.println("method mthodDeclaration:"+method.getMethodDeclaration());
		       		System.out.println("method parameters:"+node.parameters());
		       		System.out.println("method method class:"+method.getDeclaringClass().getQualifiedName());
		       		List<SingleVariableDeclaration> s = node.parameters();
		       		ArrayList<Expression> argument = new ArrayList<Expression>();
		       		for(SingleVariableDeclaration si:s)
		       		{
		       			argument.add(si.getName());
		       			
		       		}
		       		invocatiion_method.methodArguments.addAll(argument);
		       		argument.clear();
		       	
		       		
		       		
		    	}
 	 
		        return true;
		    }
		    
		  //访问ifstatement		    
		    public boolean visit(IfStatement if_node)
		    {		    	
		    	//to solve the returnStatement's bug 
		    	boolean ifNodeInInvocation = false;
	       		
	    		MyExpression myexp = new MyExpression();
	    		MyExpression myExpression = myexp.filter(if_node.getExpression(), invocatiion_method.methodArguments,invocatiion_method.methodExpression );
    			if(myExpression !=null )
    			{
    				System.out.println("the methodeclaration:"+methodDeclarNode.getName());
    				System.out.println("after filter precondition:"+myExpression.getExp().toString());
	   
    				//get the if expressions that contain return statement
	        		if(if_node.toString().contains("return"))
	        		{
	        			AST ast = AST.newAST(AST.JLS4);
	    				PrefixExpression expressionCondition = ast.newPrefixExpression();
	    				expressionCondition.setOperator(PrefixExpression.Operator.NOT);
	    				expressionCondition.setOperand((Expression)ASTNode.copySubtree(ast, myExpression.getExp()));		    		
	    				
	    				//System.out.println("return if_node:"+expressionCondition.toString());
	    				Expression afterReplace = myExpression.replaceArg(expressionCondition,invocatiion_method.methodArguments,invocatiion_method.methodExpression);
	    				invocatiion_method.methodPreCondition.add(afterReplace);
	    				//invocatiion_method.methodPreCondition.add(expressionCondition); 
	    				System.out.println("return precondition:"+afterReplace.toString());
	        			
	        		}
	    		}
		    	return true; 		   
		    }
		    
		  
		    
		    //clear the ifstatement list after visiting the method declaration nodes
		    public void endVisit(MethodDeclaration node)
		    { 
		    	ArrayList<String> condition=new ArrayList<String>();
		    	for(Expression ex:invocatiion_method.methodPreCondition) 
		    	{
		    		condition.add(ex.toString());
		    	}
		    	System.out.println("condition:"+condition.toString());
		    	
		    	try {
					csvObject.Write_CSV(condition, "E:/precondition/"+"(Class"+invocatiion_method.methodDeclarationClass+")"+invocatiion_method.methodDeclaration.toString()+".csv",fileName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	condition.clear();
		    	
		    	//clear methodlist object
		    	invocatiion_method.clear();
		    	
		    }

		
		};
		unit.accept(visitor);
	}

	
}