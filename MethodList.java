package extractif.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;

public class MethodList 
{
	public String methodPackageName;
	//public String methodExpression;  //����object.method(),object��Ϊmethod��expression
	public ArrayList<Expression> methodArguments;
	public String methodName;
	public ArrayList<Expression> methodPreCondition;
	public String methodDeclaration;
	public Expression methodExpression;
	
	MethodList()
	{
		methodPackageName = "";
		methodExpression = null;
		methodArguments = new ArrayList<Expression>();
		methodName = "";
		methodDeclaration = "";
		methodPreCondition = new ArrayList<Expression>();
		
	}
	void clear()
	{
		methodPackageName = "";
		methodExpression = null;
		methodArguments.clear();
		methodName = "";
		methodPreCondition.clear();
	}
}
