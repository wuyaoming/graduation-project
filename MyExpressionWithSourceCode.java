package extractfromsourcecode.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.InfixExpression.Operator;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NullLiteral;
import org.eclipse.jdt.core.dom.NumberLiteral;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PostfixExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.Type;

public class MyExpression 
{
	private Expression expression;
	private boolean flag;
	
	MyExpression ()
	{
		this.expression = null;
		this.flag = false;
	}
	MyExpression(Expression exp)
	{
		this.expression = exp;
		this.flag = false;
		
	}
	public Expression getExp()
	{
		return expression;
	}
	public boolean getFlag()
	{
		return flag;
	}
	public void setFlag(boolean flag)
	{
		this.flag = flag;
	}
	
	public void setExpression(Expression exp)
	{
		this.expression = exp; 
	}
	
	//contruct new MyExpression object
	public  MyExpression constructInfixExpression(MyExpression left, MyExpression right,Operator op)
	{
//		System.out.println("left expression is :"+left.toString());
//		System.out.println("right expression is :"+right.toString());
//		System.out.println("op expression is :"+op.toString());                                   
		MyExpression resultExp = new MyExpression();
		
		AST ast = AST.newAST(AST.JLS4);
		InfixExpression newInfixExpression = ast.newInfixExpression();
		newInfixExpression.setOperator(op);
		newInfixExpression.setLeftOperand((Expression)ASTNode.copySubtree(ast,left.getExp()));
		newInfixExpression.setRightOperand((Expression)ASTNode.copySubtree(ast,right.getExp()));
		
		//System.out.println("construct expression is :"+newInfixExpression.toString());
		resultExp.expression = newInfixExpression;
		if(!left.getFlag() && !right.getFlag())
			resultExp.setFlag(false);
		else
			resultExp.flag = true;
		
		return resultExp;
	}
	public MyExpression constructPostfixExpression(MyExpression oprand,PostfixExpression.Operator op)
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		PostfixExpression newPostfixExpression = ast.newPostfixExpression();
		newPostfixExpression.setOperand((Expression)ASTNode.copySubtree(ast,oprand.getExp()));
		newPostfixExpression.setOperator(op);
		resultExp.setExpression(newPostfixExpression);
		if(oprand.flag)
			resultExp.setFlag(true);
		else
			resultExp.setFlag(false);
		
		return resultExp;
		
	}
	
	public MyExpression constructPrefixExpression(MyExpression oprand,PrefixExpression.Operator op)
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		PrefixExpression newPrefixExpression = ast.newPrefixExpression();
		newPrefixExpression.setOperand((Expression)ASTNode.copySubtree(ast,oprand.getExp()));
		newPrefixExpression.setOperator(op);
		resultExp.setExpression(newPrefixExpression);
		if(oprand.flag)
			resultExp.setFlag(true);
		else
			resultExp.setFlag(false);
		
		return resultExp;
		
	}
	//construct methodinvocation
	public MyExpression constructMethodMyExpression(MyExpression methodExpression,List<Expression> methodArg,SimpleName methodName)
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		MethodInvocation newMethodInvocation = ast.newMethodInvocation();
		newMethodInvocation.setName((SimpleName)ASTNode.copySubtree(ast,methodName));
		for(int i = 0 ;i<methodArg.size();i++)
		{
			newMethodInvocation.arguments().add((Expression)ASTNode.copySubtree(ast,methodArg.get(i)));
		}
		
		newMethodInvocation.setExpression((Expression)ASTNode.copySubtree(ast,methodExpression.getExp()));
		resultExp.setExpression(newMethodInvocation);
		if(methodExpression.getFlag())
			resultExp.setFlag(true);
		else
			resultExp.setFlag(false);
		return resultExp;

	}
	//construct instanceofExpression
	public MyExpression constructInstanceMyExpression(MyExpression left,Type type)
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		InstanceofExpression newInstanceofExpression = ast.newInstanceofExpression();
		newInstanceofExpression.setLeftOperand((Expression)ASTNode.copySubtree(ast,left.getExp()));
		newInstanceofExpression.setRightOperand((Type)ASTNode.copySubtree(ast,type));
		
		resultExp.setExpression(newInstanceofExpression);
		
		if(left.getFlag())
			resultExp.setFlag(true);
		else
			resultExp.setFlag(false);
		return resultExp;
	}
	public MyExpression constructFieldAccessMyExpression(MyExpression exp,MyExpression name)
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		FieldAccess newFieldAccess = ast.newFieldAccess();
		newFieldAccess.setExpression((Expression)ASTNode.copySubtree(ast,exp.getExp()));
		newFieldAccess.setName((SimpleName)ASTNode.copySubtree(ast, name.getExp()));
		
		resultExp.setExpression(newFieldAccess);
		if(!exp.getFlag() && !name.getFlag())
			resultExp.setFlag(false);
		else
			resultExp.setFlag(true);
		//System.out.println("construct fildAccess:"+resultExp.getExp());
		return resultExp;
	}
	public MyExpression constructNullLiteralMyExpression()
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		NullLiteral newNullLiteral = ast.newNullLiteral();
		
		resultExp.setExpression(newNullLiteral);
		
		//System.out.println("construct fildAccess:"+resultExp.getExp());
		return resultExp;
	}
	
	public MyExpression constructNumberLiteralMyExpression(String literal)
	{
		MyExpression resultExp = new MyExpression();
		AST ast = AST.newAST(AST.JLS4);
		NumberLiteral newNumberLiteral = ast.newNumberLiteral(literal);
		
		resultExp.setExpression(newNumberLiteral);
		
		//System.out.println("construct fildAccess:"+resultExp.getExp());
		return resultExp;
	}
	
	//construct infixExpression function
	public Expression constructExpression(Expression left,Expression right,Operator op)
	{
		AST ast = AST.newAST(AST.JLS4);
		InfixExpression newInfixExpression = ast.newInfixExpression();
		newInfixExpression.setOperator(op);
		newInfixExpression.setLeftOperand((Expression)ASTNode.copySubtree(ast,left));
		newInfixExpression.setRightOperand((Expression)ASTNode.copySubtree(ast,right));
		return newInfixExpression;
	}
	
	public Expression constructPostfixExpression(Expression oprand,PostfixExpression.Operator op)
	{
		AST ast = AST.newAST(AST.JLS4);
		PostfixExpression newPostfixExpression = ast.newPostfixExpression();
		newPostfixExpression.setOperator(op);
		newPostfixExpression.setOperand((Expression)ASTNode.copySubtree(ast,oprand));
		
		return newPostfixExpression;
	}
	
	public Expression constructPrefixExpression(Expression oprand,PrefixExpression.Operator op)
	{
		AST ast = AST.newAST(AST.JLS4);
		PrefixExpression newPrefixExpression = ast.newPrefixExpression();
		newPrefixExpression.setOperator(op);
		newPrefixExpression.setOperand((Expression)ASTNode.copySubtree(ast,oprand));
		
		return newPrefixExpression;
	}
	
	public Expression constructMethodInvocationExpression(Expression methodExpression,List<Expression> methodArg,SimpleName methodName)
	{
		AST ast = AST.newAST(AST.JLS4);
		MethodInvocation newMethodInvocation = ast.newMethodInvocation();
		newMethodInvocation.setName((SimpleName)ASTNode.copySubtree(ast,methodName));
		for(int i = 0 ;i<methodArg.size();i++)
		{
			newMethodInvocation.arguments().add((Expression)ASTNode.copySubtree(ast,methodArg.get(i)));
		}
		//newMethodInvocation.arguments().addAll(methodArg);
		newMethodInvocation.setExpression((Expression)ASTNode.copySubtree(ast,methodExpression));
		
		return newMethodInvocation;
	}
	
	//construct InstanceofExpression
	public Expression constructInstanceofExpression(Expression left,Type type)
	{
		AST ast = AST.newAST(AST.JLS4);
		InstanceofExpression newInstanceofExpression = ast.newInstanceofExpression();
		newInstanceofExpression.setLeftOperand((Expression)ASTNode.copySubtree(ast, left));
		newInstanceofExpression.setRightOperand((Type)ASTNode.copySubtree(ast,type));
		
		return newInstanceofExpression;
	}
	
	//construct FieldAccessExpression
	public Expression constructFieldAccessExpression(Expression exp,SimpleName name)
	{
		AST ast = AST.newAST(AST.JLS4);
		FieldAccess newFieldAccessExpression = ast.newFieldAccess();
		newFieldAccessExpression.setExpression((Expression)ASTNode.copySubtree(ast, exp));
		newFieldAccessExpression.setName((SimpleName) ASTNode.copySubtree(ast, name));
		
		return newFieldAccessExpression;
	}
	
	public Expression constructNullLiteralExpression()
	{
		AST ast = AST.newAST(AST.JLS4);
		NullLiteral newNullLiteralExpression = ast.newNullLiteral();
		return newNullLiteralExpression;
	}
	
	public Expression constructNumberLiteralExpression(String literal)
	{
		AST ast = AST.newAST(AST.JLS4);
		NumberLiteral newNumberLiteralExpression = ast.newNumberLiteral(literal);
		return newNumberLiteralExpression;
	}
	//wipe off the Expressions that contain no args
	@SuppressWarnings("unchecked")
	public  MyExpression filter(Expression exp,ArrayList<Expression>args,Expression methodExpression)
	{
		//System.out.println("exp is "+exp.toString());
		if(args.size() == 0)
			return null;
		if(exp == null)
			return null;
		//falg that indicate if expressions contain args or method expressions
		boolean contain_arg =false;
		boolean contain_methodExpression = false;
		
		for(int index = 0;index <args.size();index++)
		{
			if(exp.toString().trim().contains(args.get(index).toString()))
			{
				contain_arg = true;		
			}
		}
		
		
		
		if(!contain_arg )
			return null;
		
		
		//InfixExpression
		if(exp instanceof InfixExpression)
		{
			
			Expression left = ((InfixExpression) exp).getLeftOperand();
			Expression right = ((InfixExpression) exp).getRightOperand();
			MyExpression newLeft;
			MyExpression newRight;
			//if the left or right expression is null or number
			if(left instanceof NullLiteral)
				newLeft = constructNullLiteralMyExpression();
			else if(left instanceof NumberLiteral)
				newLeft = constructNumberLiteralMyExpression(((NumberLiteral) left).getToken());
			else
				newLeft = filter(left,args,methodExpression);
			//System.out.println("left is "+left.toString());

			if(right instanceof NullLiteral)
				newRight = constructNullLiteralMyExpression();
			else if(right instanceof NumberLiteral)
				newRight = constructNumberLiteralMyExpression(((NumberLiteral) right).getToken());
			else 
				newRight = filter(right,args,methodExpression);
			
			
			Operator op = ((InfixExpression) exp).getOperator();
			
			if(newLeft == null && newRight == null)
				return null;            
			else if (newLeft !=null && newRight == null )
			{
				if(newLeft.getFlag())
					return newLeft;
				else
					return null;
			}
	
			else if(newLeft == null && newRight != null)
			{
				if(newRight.getFlag())
					return newRight;
				else
					return null;
			}				
			else 
			{
				//System.out.println("op is :"+op.toString());
				if(op.equals(InfixExpression.Operator.CONDITIONAL_OR) || op.equals(InfixExpression.Operator.CONDITIONAL_AND) /*|| op.equals(InfixExpression.Operator.EQUALS) ||op.equals(InfixExpression.Operator.LESS)||op.equals(InfixExpression.Operator.PLUS*/)
				
				{
					//System.out.println("come in");
					if(newLeft.getFlag() == true && newRight.getFlag() == true)
						return constructInfixExpression(newLeft,newRight,op);
					else if(newLeft.getFlag() == true && newRight.getFlag() == false)
						return newLeft;						
					else if(!newLeft.getFlag() == false && newRight.getFlag() == true)
						return newRight;
					else
						return null;	
				}
				else 
				{
					if(!newLeft.getFlag() && !newRight.getFlag())
						return null;
					else
						return constructInfixExpression(newLeft,newRight,op);
				}
			}	
		}
		//ParenthesizedExpression
		else if(exp instanceof ParenthesizedExpression)			
		{
			return filter(((ParenthesizedExpression) exp).getExpression(),args,methodExpression);
		}
		//PostfixExpression
		else if(exp instanceof PostfixExpression)
		{
			Expression oprand = ((PostfixExpression) exp).getOperand();
			PostfixExpression.Operator op = ((PostfixExpression) exp).getOperator();
			
			MyExpression newMyExp = filter(oprand,args,methodExpression);
			if(newMyExp == null)
				return null;
			else 
			{	
				if(newMyExp.getFlag() == true)
					return constructPostfixExpression(newMyExp,op);	
				else
					return null;
			}
		}
		//prefixExpression
		else if(exp instanceof PrefixExpression)
		{
			Expression oprand = ((PrefixExpression) exp).getOperand();
			PrefixExpression.Operator op = ((PrefixExpression) exp).getOperator();
			
			MyExpression newMyExp = filter(oprand,args,methodExpression);
			if(newMyExp == null)
				return null;
			else 
			{	
				if(newMyExp.getFlag() == true)
					return constructPrefixExpression(newMyExp,op);	
				else
					return null;
			}
		}
		else if(exp instanceof MethodInvocation)
		{
			Expression methodInvocation = ((MethodInvocation) exp).getExpression();
			List<Expression> methodArgList = new ArrayList<Expression>();
			methodArgList.addAll(((MethodInvocation) exp).arguments());
			SimpleName methodName = ((MethodInvocation) exp).getName();
			
			MyExpression newMyExp = filter(methodInvocation,args,methodExpression);
			
			if(newMyExp == null)
				return null;
			else
			{
				if(newMyExp.getFlag() == true)
				{
					//System.out.println("Arg is :"+methodArgList);
					return constructMethodMyExpression(newMyExp,methodArgList,methodName);
				}
				else
					return null;
			}
		}
		
		else if(exp instanceof InstanceofExpression)
		{
			Expression left = ((InstanceofExpression) exp).getLeftOperand();
			MyExpression newMyExp = filter(left,args,methodExpression);
			
			if(newMyExp == null)
				return null;
			else
			{
				if(newMyExp.getFlag() == true)
				{
					return constructInstanceMyExpression(newMyExp,((InstanceofExpression) exp).getRightOperand());
				}
				else
					return null;
					
			}
			
		}
		
		else if(exp instanceof FieldAccess)
		{
			Expression expression = ((FieldAccess) exp).getExpression();
			Expression name = ((FieldAccess) exp).getName();
			MyExpression newExp = filter(expression ,args,methodExpression);
			MyExpression newName = filter(name,args,methodExpression);
			
			if(newExp == null && newName ==null)
			{
				return null;
			}
			else if(newExp != null && newName == null)
				return newExp;
			else if(newExp ==null && newName !=null)
				return newName;
			else
			{
				//System.out.println("come in ");
				if(!newName.getFlag() && !newExp.getFlag())
				{
					//System.out.println("constructing !");
					return null;
				}
					
				else					
					return constructFieldAccessMyExpression(newExp,newName);
			}			
		}

		//SimpleName
		else if(exp instanceof Name)
		{
			if(exp instanceof SimpleName)
			{
				MyExpression mexp = new MyExpression(exp);
				for(int index = 0;index <args.size();index++)
				{
					if(((SimpleName) exp).getIdentifier().trim().toString().equals(args.get(index).toString()))
					{
						mexp.setFlag(true);		
					}
				}
				
					
				//System.out.println("mexp:"+mexp.getExp().toString()+"       flag:"+mexp.getFlag());
				return mexp;
				
			}
		//QualifiedName
			else if(exp instanceof QualifiedName)
			{
				MyExpression mexp = new MyExpression(exp);
				for(int index = 0;index<args.size();index ++)
				{
					if(((QualifiedName) exp).getQualifier().toString().trim().equals(args.get(index).toString()))
					{
						mexp.setFlag(true);		
					}
				}
				if(methodExpression !=null)
				{
					if(((QualifiedName) exp).getQualifier().toString().trim().equals(methodExpression.toString()))
					{
						mexp.setFlag(true);		
					}
					
				}
				return mexp;
			}
			else 
			{
				MyExpression temp= new MyExpression(exp);
				//System.out.println("exp:"+temp.getExp().toString()+"      flag:"+temp.getFlag());
				return temp;
			}
		}
		
		else 
		{
			MyExpression temp= new MyExpression(exp);
			//System.out.println("exp:"+temp.getExp().toString()+"      flag:"+temp.getFlag());
			return temp;
		}
			
	}
	
	public Expression replaceArg(Expression exp,ArrayList<Expression> args,Expression methodExpression)
	{
		if(exp instanceof InfixExpression)
		{
			Expression left =((InfixExpression) exp).getLeftOperand();
			Expression right = ((InfixExpression) exp).getRightOperand();
			Expression newLeft;
			Expression newRight;
			if(left instanceof NullLiteral)
				newLeft = constructNullLiteralExpression();
			else if(left instanceof NumberLiteral)
				newLeft = constructNumberLiteralExpression(((NumberLiteral)left).getToken());
			else 
				newLeft = replaceArg(left,args,methodExpression);
			if(right instanceof NullLiteral)
				newRight = constructNullLiteralExpression();
			else if(right instanceof NumberLiteral)
				newRight = constructNumberLiteralExpression(((NumberLiteral )right).getToken());
			else
				newRight = replaceArg(right,args,methodExpression);
			Operator op = ((InfixExpression) exp).getOperator();
			return constructExpression(newLeft,newRight,op);
		}
		else if(exp instanceof PostfixExpression)
		{
			Expression oprand = ((PostfixExpression) exp).getOperand();
			PostfixExpression.Operator op = ((PostfixExpression) exp).getOperator();
			Expression newOprand = replaceArg(oprand,args,methodExpression);
			return constructPostfixExpression(newOprand,op);
		}
		else if(exp instanceof PrefixExpression)
		{
			Expression oprand = ((PrefixExpression) exp).getOperand();
			PrefixExpression.Operator op = ((PrefixExpression) exp).getOperator();
			Expression newOprand = replaceArg(oprand,args,methodExpression);
			return constructPrefixExpression(newOprand,op);
		}
		else if(exp instanceof MethodInvocation)
		{
			Expression methodExpressions = ((MethodInvocation) exp).getExpression();
			List<Expression> methodArgList = ((MethodInvocation) exp).arguments();
			SimpleName methodName = ((MethodInvocation) exp).getName();
			
			Expression newMethodExpression = replaceArg(methodExpressions,args,methodExpression);
			return constructMethodInvocationExpression(newMethodExpression,methodArgList,methodName);
		}
		else if(exp instanceof InstanceofExpression)
		{
			Expression left = ((InstanceofExpression) exp).getLeftOperand();
			
			Expression newleft = replaceArg(left,args,methodExpression);
			
			return  constructInstanceofExpression(newleft,((InstanceofExpression) exp).getRightOperand());
		}
		else if(exp instanceof FieldAccess)
		{
			Expression expression = ((FieldAccess) exp).getExpression();
			Expression name = ((FieldAccess) exp).getName();
			
			Expression newExpresison = replaceArg(expression,args,methodExpression);
			Expression newName = replaceArg(name,args,methodExpression);
			
			return constructFieldAccessExpression(newExpresison,(SimpleName)newName);
		}
		
		else if(exp instanceof SimpleName)
		{
			for(int index = 0 ;index <args.size();index ++)
			{
				if(((SimpleName) exp).getIdentifier().trim().toString().equals(args.get(index).toString()))
				{
					((SimpleName) exp).setIdentifier("arg"+(int)(index+1));
					return exp;
				}
				
			}
			/*
			if(methodExpression !=null)
			{
				if(((SimpleName) exp).getIdentifier().trim().toString().equals(methodExpression.toString()))
				{
					((SimpleName) exp).setIdentifier("rev");
					return exp;
				}
			}*/
		}		
		return exp;
	}
}
