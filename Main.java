
package extractif.handlers;

import java.io.IOException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jface.dialogs.MessageDialog;

import extractif.handlers.DoCreateAST;
import extractif.handlers.DoVisitAST;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class Main extends AbstractHandler {
	/**
	 * The constructor.
	 */
	public Main() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		//IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
	    IWorkspaceRoot root = workspace.getRoot();
	    //System.out.println(root.isAccessible());
	    //System.out.println(root.getLocation());
	    //IProject projects = root.getProject("apache-log4j-2.10.0-src");
	    //IProject projects = root.getProject("testProject");
	    IProject projects = root.getProject("project");
	    //root.getp
	    //System.out.println(projects.isAccessible());
	    	
	    IJavaProject javaProject = JavaCore.create(projects);
	    
	    DoCreateAST createObject = new DoCreateAST();
	    DoVisitAST visitObject = new DoVisitAST();
	    
	    System.out.println();	  
	    System.out.println("projcet's name is:"+javaProject.getElementName());
	    System.out.println("dealing....");

	 
		try {
			IPackageFragment[] package_ = javaProject.getPackageFragments();
			System.out.println("length:"+package_.length);
		    for (IPackageFragment packages : package_)
		    {
		    	if(packages.getKind() != IPackageFragmentRoot.K_SOURCE)
				{
					continue;
				}
				ICompilationUnit[] units = packages.getCompilationUnits();
				System.out.println("length:"+units.length);
				if(units.length ==0)
					continue;
				
				for(ICompilationUnit unit:units)
				{
					CompilationUnit c = createObject.CreatAST(unit);
					visitObject.visitAST(c,unit.getElementName());	
				}
		    }
		} catch (JavaModelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("done!!!!!!!!!!!!!!!!");
		return null;
	}
}
