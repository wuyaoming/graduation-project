package extractif.handlers;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.IfStatement;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.PrefixExpression;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.ThrowStatement;
import org.eclipse.jdt.core.dom.WhileStatement;









import java.util.*;

public class DoVisitAST <visit_AST_Type>
{
	
	private List funList ;
	private ArrayList<MethodList> methodlists;
	private ArrayList<String> packagelist;
	private CSVUtil csvObject;
	DoVisitAST()
	{
		csvObject = new CSVUtil();
		funList = new ArrayList();
		methodlists = new ArrayList<MethodList>();
		packagelist = new ArrayList<String>();
		
		packagelist.add("java.lang.String");
		packagelist.add("java.util.ArrayList");
		packagelist.add("java.util.LinkedList");
		packagelist.add("java.util.HashSet");
		packagelist.add("java.util.HashMap");
		packagelist.add("java.util.Stack");
		packagelist.add("java.util.Vector");
		packagelist.add("java.util.LinkedHashMap");
		packagelist.add("java.util.LinkedHashSet");
		packagelist.add("java.util.Iterator");
		packagelist.add("java.util.ListIterator");
	}
	
	public boolean judgePackage(String packagename)
	{
		
		for(int i = 0;i<packagelist.size();i++)
		{
			if(packagename.trim().startsWith(packagelist.get(i)))
				return true;	
		}
		return false;
	}
	
	//visit AST
	public void visitAST(final CompilationUnit unit,final String fileName)
	{
		//ArrayList<MyMethod> classMethodList = new ArrayList<MyMethod>();
		final MethodList invocatiion_method = new MethodList();
		ASTVisitor visitor = new ASTVisitor() 
		{
			public ArrayList<IfStatement> ifList = new ArrayList<IfStatement>();
			//访问函数声明
		    public boolean visit(MethodDeclaration node) 
		    {
		    	String caller = node.getName().toString(); 
		    	
		    	//test content
		        System.out.println("函数: " + caller);
		        return true;
		    }
		    
		    //访问ifstatement		    
		    public boolean visit(IfStatement if_node)
		    {
		    	ifList.add(if_node);
		 
		    	return true;
		    }
		   

		    //访问invaction
		    public boolean visit(final MethodInvocation node) 
		    {
		
		       	IMethodBinding method = (IMethodBinding)node.resolveMethodBinding();
		       	if(method != null &&method.getDeclaringClass() != null && method.getDeclaringClass().getQualifiedName().toString().trim().startsWith("java."))
		    	{
		    	 
		       		
		       		if(node.getExpression() != null)     //若有objedt.method(),则加上rcv.
		    		{
		       			invocatiion_method.methodExpression = node.getExpression();
		    			 
		    		}		    		 
		       		invocatiion_method.methodName = node.getName().toString();
		       		invocatiion_method.methodPackageName = method.getDeclaringClass().getQualifiedName().toString();
		       		invocatiion_method.methodDeclaration = method.getMethodDeclaration().toString();
		       		invocatiion_method.methodArguments.addAll(node.arguments());
		    		 
		       		System.out.println("method invocation:"+node.toString());
		       		//用来解决return的bug
		       		ArrayList<IfStatement> parentIfNodes = new ArrayList<IfStatement>();
		    		for (final IfStatement if_node:ifList)
		    		{
		    			MyExpression myexp = new MyExpression();
		    			parentIfNodes.add(if_node);
		    			//filter the unconnected ifStatement conditions
		    			MyExpression myExpression = myexp.filter(if_node.getExpression(), invocatiion_method.methodArguments,invocatiion_method.methodExpression );
		    			if(myExpression !=null)
		    			{
		    				System.out.println("after filter precondition:"+myExpression.getExp().toString());
			    			
		    				if(if_node.getThenStatement() != null && if_node.getThenStatement().toString().contains(node.toString()))
			    			{	
				        		if((if_node.getElseStatement() != null && !if_node.getElseStatement().toString().contains(node.getName().toString()))
				        					 || if_node.getElseStatement()==null)
				        		{
				        			Expression afterReplace = myExpression.replaceArg(myExpression.getExp(),invocatiion_method.methodArguments,invocatiion_method.methodExpression);
				    				invocatiion_method.methodPreCondition.add(afterReplace);
				        			//invocatiion_method.methodPreCondition.add(if_node.getExpression()); 
			        				System.out.println("precondition:"+afterReplace.toString());
								
				        		}
			    			}
			        		else if(if_node.getElseStatement() != null && if_node.getElseStatement().toString().contains(node.toString()))
				        	{ 

				        			AST ast = AST.newAST(AST.JLS4);
				    				PrefixExpression expressionCondition = ast.newPrefixExpression();
				    				expressionCondition.setOperator(PrefixExpression.Operator.NOT);
				    				expressionCondition.setOperand((Expression)ASTNode.copySubtree(ast,myExpression.getExp()));		    		
				    				
				    				Expression afterReplace = myExpression.replaceArg(expressionCondition,invocatiion_method.methodArguments,invocatiion_method.methodExpression);
				    				invocatiion_method.methodPreCondition.add(afterReplace);
				    				//invocatiion_method.methodPreCondition.add(expressionCondition); 
				    				System.out.println("precondition:"+afterReplace.toString());
				        	}
		    				//get the if expressions that contain return statement
			        		if(!if_node.toString().contains(node.toString()) && if_node.toString().contains("return"))
			        		{
			        			boolean return_contains = false;
			        			for(IfStatement parentIfNode:parentIfNodes)
			        			{
			        				if(parentIfNode.toString().contains(node.toString()))
			        				{
			        					return_contains = true;
			        					break;
			        				}
			        					
			        			}
			        			if(!return_contains)
			        			{
				        			AST ast = AST.newAST(AST.JLS4);
				    				PrefixExpression expressionCondition = ast.newPrefixExpression();
				    				expressionCondition.setOperator(PrefixExpression.Operator.NOT);
				    				expressionCondition.setOperand((Expression)ASTNode.copySubtree(ast, myExpression.getExp()));		    		
				    				
				    				//System.out.println("return if_node:"+expressionCondition.toString());
				    				Expression afterReplace = myExpression.replaceArg(expressionCondition,invocatiion_method.methodArguments,invocatiion_method.methodExpression);
				    				invocatiion_method.methodPreCondition.add(afterReplace);
				    				//invocatiion_method.methodPreCondition.add(expressionCondition); 
				    				System.out.println("return precondition:"+afterReplace.toString());
			        			}
			        		}

		    			}

		    		}
	 
		    	}
 	 
		        return true;
		    }
		    
		    //write the preconditions into csv file after visiting the methodvocation nodes
		    public void endVisit(MethodInvocation node)
		    {
		    	
		    	ArrayList<String> condition=new ArrayList<String>();
		    	for(Expression ex:invocatiion_method.methodPreCondition) 
		    	{
		    		condition.add(ex.toString());
		    	}
		    	System.out.println("condition:"+condition.toString());
		    	//int size = condition.size();
		    	//String[] dataToWrite = condition.toArray();
		    	try {
					csvObject.Write_CSV(condition, "D:/"+invocatiion_method.methodDeclaration.toString()+".csv",fileName);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	condition.clear();
		    	
		    	//clear methodlist object
		    	invocatiion_method.clear();
		    	
		    }
		    
		    //clear the ifstatement list after visiting the method declaration nodes
		    public void endVisit(MethodDeclaration node)
		    {
		    	ifList.clear();
		    	//----------------------------test content---------------------
		    	//for (MethodList methodlist:methodlists)
		    	//{
		    		//System.out.println("method's name is :"+methodlist.methodName);
			    	//System.out.println("method's expression is:"+methodlist.methodExpression);
	    			//System.out.println("method's packagename is:"+methodlist.methodPackageName);
	    			//System.out.println("method's argument is:"+methodlist.methodArguments.toString());
	    			//System.out.println("method's if_condition_list is:"+methodlist.ifConditionList.toString());
	    			//methodlist.clear();
		    	//}
		    	//---------------------------test content end---------------------
    	
    			
		    }

		
		};
		unit.accept(visitor);
	}

	
}