package extractif.handlers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;


public class CSVUtil 
{
	
	public CSVUtil()
	{
		
	}
	public void Write_CSV(ArrayList<String> data,String fileName,String conditionFileName) throws IOException
	{
		if(data.size() == 0)
			return;
		File csvFile = new File(fileName);
		
        BufferedWriter bw = new BufferedWriter(new FileWriter(csvFile,true));
        String toWrite = "";
        for(String dataString:data)
        {
        	toWrite = toWrite + dataString+",";
        }
        //toWrite = toWrite+conditionFileName;
        //bw.write(toWrite);
        int length = toWrite.length();
        bw.write(toWrite.substring(0, length-1));
        
        toWrite="";
        bw.newLine();
        bw.close();

	}
	

}
